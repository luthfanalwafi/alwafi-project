import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/db_const.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/constants/loading.dart';
import 'package:finance_apps/widgets/edit_form.dart';
import 'package:finance_apps/widgets/in_out_form.dart';
import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';

class CashFlow extends StatefulWidget {
  final phoneNumber;
  final jenis;

  CashFlow({
    this.jenis,
    this.phoneNumber,
  });

  @override
  _CashFlowState createState() => _CashFlowState();
}

class _CashFlowState extends State<CashFlow> {
  var phoneNumber;
  String userName;
  bool isLoading = false;
  List listData = [];
  final CollectionReference collection =
      FirebaseFirestore.instance.collection(kCashFlowTable);
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection(kUserTable);

  @override
  void initState() {
    super.initState();
    phoneNumber = widget.phoneNumber;
    getCashFlow();
  }

  Future getCashFlow() async {
    isLoading = true;
    listData.clear();
    await collection.get().then((value) {
      value.docs.forEach((element) {
        if (widget.jenis != S().semua) {
          if (element.data()[S().tipe] == widget.jenis) {
            listData.add(element.data());
          }
        } else {
          listData.add(element.data());
        }
      });
    });
    await userCollection.doc(phoneNumber).get().then((value) {
      var userProfile = value.data();
      userName = userProfile[S().nama];
    });
    setState(() {
      isLoading = false;
    });
  }

  Future onDelete(docID) async {
    await collection.doc(docID).delete();
    getCashFlow();
  }

  void addDataDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          content: InOutForm(
            tipe: widget.jenis,
            user: userName,
            onSubmit: getCashFlow,
          ),
        );
      },
    );
  }

  void onTapDialog(element) {
    var docID = DateFormat(kDateFormatDB).format(
      DateFormat(kDateFormatField).parse(element[S().ditambahkanPada]),
    );
    var keperluan = element[S().keperluan];
    var jumlah = element[S().jumlah];
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          insetPadding: EdgeInsets.symmetric(horizontal: 80.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              MaterialButton(
                height: 50.0,
                child: Container(
                  padding: EdgeInsets.only(left: 10.0),
                  alignment: Alignment.centerLeft,
                  child: Text(S().ubah),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  editDataDialog(
                    docID,
                    keperluan,
                    jumlah,
                  );
                },
              ),
              Divider(
                height: 1,
              ),
              MaterialButton(
                height: 50.0,
                child: Container(
                  padding: EdgeInsets.only(left: 10.0),
                  alignment: Alignment.centerLeft,
                  child: Text(S().hapus),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  deleteDataDialog(docID);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  void editDataDialog(docID, keperluan, jumlah) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          content: EditForm(
            tipe: widget.jenis,
            docID: docID,
            keperluan: keperluan,
            jumlah: jumlah,
            onSubmit: getCashFlow,
          ),
        );
      },
    );
  }

  void deleteDataDialog(docID) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text(S().hapusData),
          content: Text(S().konfirmasiHapus),
          actions: [
            MaterialButton(
              child: Text(
                S().gajadi,
                style: TextStyle(
                  color: kColorPrimary,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            MaterialButton(
              child: Text(
                S().hapus,
                style: TextStyle(
                  color: kColorPrimary,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  isLoading = true;
                });
                onDelete(docID);
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? kLoadingWidget(context)
        : Scaffold(
            body: GroupedListView<dynamic, String>(
              elements: listData,
              groupBy: (element) {
                String date = DateFormat(kDateFormat).format(
                  DateFormat(kDateFormat).parse(element[S().ditambahkanPada]),
                );
                return date;
              },
              groupComparator: (a, b) {
                String dateA = DateFormat(kDateFormatDB).format(
                  DateFormat(kDateFormat).parse(a),
                );
                String dateB = DateFormat(kDateFormatDB).format(
                  DateFormat(kDateFormat).parse(b),
                );
                return dateA.compareTo(dateB);
              },
              itemComparator: (a, b) {
                String dateA = a[S().ditambahkanPada];
                String dateB = b[S().ditambahkanPada];
                return dateA.compareTo(dateB);
              },
              order: GroupedListOrder.DESC,
              useStickyGroupSeparators: true,
              separator: Container(
                color: Colors.white,
                child: Divider(),
              ),
              groupSeparatorBuilder: (String value) {
                String title = DateFormat(kDateFormat).format(
                  DateFormat(kDateFormat).parse(value),
                );
                return Container(
                  color: kDarkBgLight,
                  padding: EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 15.0,
                  ),
                  child: Text(title),
                );
              },
              itemBuilder: (c, element) {
                return ListTile(
                  title: Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Text(element[S().keperluan]),
                  ),
                  subtitle: Text(element[S().ditambahkanOleh]),
                  trailing: element[S().tipe] == S().pemasukan
                      ? Text(
                          '+ ' + kFormatCurrency.format(element[S().jumlah]),
                          style: TextStyle(
                            color: kColorPrimary,
                          ),
                        )
                      : Text(
                          '- ' + kFormatCurrency.format(element[S().jumlah]),
                          style: TextStyle(
                            color: Colors.red,
                          ),
                        ),
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
                  tileColor: Colors.white,
                  onTap: () {
                    onTapDialog(element);
                  },
                );
              },
            ),
            floatingActionButton: widget.jenis != S().semua
                ? FloatingActionButton(
                    child: Icon(Icons.add_sharp),
                    backgroundColor: kColorPrimary,
                    splashColor: Colors.white,
                    onPressed: () {
                      addDataDialog();
                    },
                  )
                : Container(),
          );
  }
}
