import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/db_const.dart';
import 'package:finance_apps/constants/images.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/constants/loading.dart';
import 'package:finance_apps/constants/styles.dart';
import 'package:finance_apps/widgets/edit_profile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class UserProfile extends StatefulWidget {

  final User user;

  UserProfile({
    this.user,
  });

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  var peran = '', phone = '';
  var ddValue;
  bool isLoading = false;
  bool isContentChanging = false;
  Map<String, dynamic> userProfile;
  double fontSize = 16.0;
  TextEditingController nameController = TextEditingController();
  final dateFormat = DateFormat(kDateFormat);
  DateTime tanggalLahir;
  final CollectionReference collection =
      FirebaseFirestore.instance.collection(kUserTable);

  @override
  void initState() {
    super.initState();
    phone = widget.user.phoneNumber;
    getProfile();
  }

  Future getProfile() async {
    isLoading = true;
    await collection.doc(phone).get().then((value) {
      userProfile = value.data();
      tanggalLahir = dateFormat.parse(userProfile[S().tanggalLahir]);
      nameController.text = userProfile[S().nama];
      peran = userProfile[S().peran];
    });
    setState(() {
      isLoading = false;
    });
  }

  Future updateProfile(title, data) async {
    isContentChanging = true;
    showLoading(title);
    await collection.doc(phone).update({
      title: data,
    });
    getProfile();
    Future.delayed(Duration(seconds: 1), () {
      isContentChanging = false;
      showLoading(title);
    });
  }

  void showLoading(title) {
    if (isContentChanging) {
      showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height,
            child: kLoadingWidget(context),
          );
        },
      );
    } else {
      Navigator.of(context).pop();
      showSnackBar(title);
    }
  }

  void showSnackBar(title) {
    final snackbar = SnackBar(
      content: Text(
        title + ' ' + S().berhasilDiubah,
        style: TextStyle(
          fontSize: fontSize,
        ),
      ),
      backgroundColor: kColorPrimary,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackbar);
  }

  void editField(title, content, type) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: kBottomSheet,
      builder: (context) {
        return EditProfile(
          nameController: nameController,
          peran: peran,
          title: title,
          type: type,
          onClick: updateProfile,
        );
      },
    );
  }

  Future<void> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: tanggalLahir,
      firstDate: DateTime(1945, 1),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != tanggalLahir) {
      setState(() {
        tanggalLahir = picked;
      });
      var date = dateFormat.format(tanggalLahir);
      print(date);
      updateProfile(S().tanggalLahir, date);
    }
  }

  @override
  Widget build(BuildContext context) {
    // double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final foto = Container(
      padding: EdgeInsets.symmetric(vertical: 30.0),
      alignment: Alignment.center,
      // color: Colors.grey,
      child: CircleAvatar(
        radius: screenHeight * 0.07,
        backgroundImage: AssetImage(kFotoMama),
        backgroundColor: Colors.black,
      ),
    );

    Column form(title, content, type, editable) {
      return Column(
        children: [
          ListTile(
            title: Container(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: fontSize,
                ),
              ),
            ),
            subtitle: Text(
              content,
              style: TextStyle(
                fontSize: fontSize,
              ),
            ),
            trailing: editable
                ? InkWell(
                    child: Text(
                      S().ubah,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: kColorPrimary,
                        fontSize: fontSize,
                      ),
                    ),
                    onTap: () {
                      if (type == 'DatePicker') {
                        selectDate(context);
                      } else {
                        editField(title, content, type);
                      }
                    })
                : Container(
                    child: Text(''),
                  ),
          ),
          Divider(),
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(S().profile),
      ),
      body: isLoading
          ? kLoadingWidget(context)
          : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                  bottom: 30.0,
                  left: 5.0,
                  right: 5.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    foto,
                    form(
                      S().noHP,
                      userProfile[S().noHP],
                      S().tidakAdaData,
                      false,
                    ),
                    form(
                      S().id,
                      userProfile[S().id],
                      S().tidakAdaData,
                      false,
                    ),
                    form(
                      S().nama,
                      userProfile[S().nama],
                      S().textField,
                      true,
                    ),
                    form(
                      S().peran,
                      userProfile[S().peran],
                      S().dropDown,
                      true,
                    ),
                    form(
                      S().tanggalLahir,
                      userProfile[S().tanggalLahir],
                      S().datePicker,
                      true,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
