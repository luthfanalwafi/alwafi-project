import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/db_const.dart';
import 'package:finance_apps/constants/images.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/constants/loading.dart';
import 'package:finance_apps/constants/styles.dart';
import 'package:flutter/material.dart';

class AturSaldo extends StatefulWidget {
  final user;

  AturSaldo({
    this.user,
  });

  @override
  _AturSaldoState createState() => _AturSaldoState();
}

class _AturSaldoState extends State<AturSaldo> {
  bool isLoading = false;
  int saldo = 0;
  List listPemasukan = [];
  List listPengeluaran = [];
  final CollectionReference collection =
      FirebaseFirestore.instance.collection(kCashFlowTable);

  @override
  void initState() {
    super.initState();
    getSaldo();
  }

  Future getSaldo() async {
    isLoading = true;
    await collection.get().then((value) {
      value.docs.forEach((element) {
        if (element.data()[S().tipe] == S().pemasukan) {
          listPemasukan.add(element.data());
        } else {
          listPengeluaran.add(element.data());
        }
      });
      for (int i = 0; i < listPemasukan.length; i++) {
        saldo = saldo + listPemasukan[i][S().jumlah];
      }
      for (int i = 0; i < listPengeluaran.length; i++) {
        saldo = saldo - listPengeluaran[i][S().jumlah];
      }
    });
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {

    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: isLoading
          ? kLoadingWidget(context)
          : Container(
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(kBackgroundGreen),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                            top: 70.0,
                            bottom: 20.0,
                            left: 20.0,
                            right: 20.0,
                          ),
                          child: ListTile(
                            title: Padding(
                              padding: EdgeInsets.only(
                                bottom: 10.0,
                              ),
                              child: Text(
                                S().saldo,
                                style: TextStyle(
                                  fontSize: 18.0,
                                ),
                              ),
                            ),
                            subtitle: Text(
                              kFormatCurrency.format(saldo),
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: 10.0,
                            bottom: 20.0,
                            left: 20.0,
                            right: 20.0,
                          ),
                          child: ListTile(
                            title: Padding(
                              padding: EdgeInsets.only(
                                bottom: 10.0,
                              ),
                              child: Text(
                                S().saldoNganggur,
                                style: TextStyle(
                                    // fontSize: 18.0,
                                    ),
                              ),
                            ),
                            subtitle: Text(
                              'Rp 10.000.000',
                              style: TextStyle(
                                // fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(15.0),
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        width: MediaQuery.of(context).size.width - 70,
                        padding: EdgeInsets.symmetric(
                          vertical: 10.0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(
                              Icons.add,
                              size: 18.0,
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                left: 20.0,
                              ),
                              child: Text(
                                S().tambahTabungan,
                                style: TextStyle(
                                  fontSize: 12.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // Card(
                  //   child: Container(
                  //     width: screenWidth - 70,
                  //     padding: EdgeInsets.all(10.0),
                  //     child: ListTile(
                  //       leading: Container(
                  //         height: screenHeight,
                  //         margin: EdgeInsets.only(
                  //           right: 5.0,
                  //         ),
                  //         width: 50.0,
                  //         decoration: ShapeDecoration(
                  //           color: kColorPrimary.withOpacity(0.3),
                  //           shape: kCircleContainer,
                  //         ),
                  //         child: Icon(
                  //           Icons.home,
                  //           // size: screenHeight * 0.5,
                  //           color: kColorPrimary,
                  //         ),
                  //       ),
                  //       title: Container(
                  //         padding: EdgeInsets.only(
                  //           bottom: 10.0,
                  //         ),
                  //         child: Text('Beli Rumah'),
                  //       ),
                  //       subtitle: Text('Rp. 2.000.000'),
                  //     )
                  //   ),
                  // ),
                ],
              ),
            ),
    );
  }
}
