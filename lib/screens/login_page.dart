// import 'package:country_code_picker/country_code_picker.dart';
import 'package:country_code_picker/country_code.dart';
import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/images.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/constants/styles.dart';
import 'package:finance_apps/helper/login_store.dart';
import 'package:finance_apps/widgets/loader_hud.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  CountryCode countryCode;
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  // bool _isObsecure = true;
  String phoneNumber, _phone;

  @override
  void initState() {
    super.initState();
    _phone = '';
    countryCode = CountryCode(
      code: 'ID',
      dialCode: '+62',
      name: 'Indonesia',
    );
    phoneController.addListener(
      () {
        if (phoneController.text != _phone && phoneController.text != '') {
          _phone = phoneController.text;
          onPhoneNumberChange(
            _phone,
            '${countryCode.dialCode}$_phone',
            countryCode.code,
          );
        }
      },
    );
  }

  void onPhoneNumberChange(
    String number,
    String internationalizedPhoneNumber,
    String isoCode,
  ) {
    if (internationalizedPhoneNumber.isNotEmpty) {
      phoneNumber = internationalizedPhoneNumber;
    } else {
      phoneNumber = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginStore>(
      builder: (_, loginStore, __) {
        _login() {
          if (phoneNumber != null) {
            if (phoneNumber.substring(3).startsWith('0')) {
              phoneNumber = '+62' + phoneNumber.substring(4);
            }
            loginStore.getCodeWithPhoneNumber(
              context,
              phoneNumber.toString(),
            );
          } else {
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                AlertDialog alert = AlertDialog(
                  title: Text(S().gagal),
                  content: Text(
                    S().noPhoneNumber,
                    style: TextStyle(
                      height: 1.5,
                    ),
                  ),
                  actions: <Widget>[
                    MaterialButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        S().ok,
                        style: TextStyle(
                          color: kColorPrimary,
                        ),
                      ),
                    ),
                  ],
                );
                return alert;
              },
            );
          }
        }

        Widget welcomeLabel = Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(
            top: 30.0,
            left: 30.0,
            right: 30.0,
          ),
          child: Text(
            S().selamatDatang,
            style: TextStyle(
              fontSize: 15.0,
            ),
          ),
        );

        Widget phoneNumberTextField = Container(
          padding:
              EdgeInsets.only(top: 20.0, bottom: 20.0, left: 30.0, right: 30.0),
          child: TextFormField(
            controller: phoneController,
            decoration: kTextField.copyWith(
              hintText: S().noHP,
              prefixIcon: Icon(Icons.phone_android),
            ),
            cursorColor: kColorPrimary,
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp("[0-9]"))
            ],
            keyboardType: TextInputType.number,
            autocorrect: false,
            validator: (val) => val.isEmpty ? 'Email can\'t be empty.' : null,
            // onSaved: (val) => _email = val,
          ),
        );

        // Widget passwordTextField = Container(
        //   padding: EdgeInsets.only(
        //     left: 30.0,
        //     right: 30.0,
        //   ),
        //   child: Theme(
        //     data: ThemeData(
        //       primaryColor: kColorPrimary,
        //     ),
        //     child: TextFormField(
        //       key: Key('password'),
        //       decoration: kTextField.copyWith(
        //         hintText: S().password,
        //         prefixIcon: Icon(Icons.lock),
        //       ),
        //       controller: passwordController,
        //       cursorColor: kColorPrimary,
        //       obscureText: _isObsecure,
        //       autocorrect: false,
        //       validator: (val) =>
        //           val.isEmpty ? 'Password can\'t be empty.' : null,
        //       // onSaved: (val) => _password = val,
        //     ),
        //   ),
        // );

        // Widget forgotPassword = Container(
        //   // width: 150.0,
        //   // height: 70.0,
        //   alignment: Alignment.centerRight,
        //   padding: EdgeInsets.only(
        //     bottom: 10.0,
        //     right: 20.0,
        //   ),
        //   child: MaterialButton(
        //     shape: RoundedRectangleBorder(
        //       borderRadius: BorderRadius.circular(10.0),
        //     ),
        //     child: Text(
        //       S().lupaPassword,
        //       style: TextStyle(
        //         fontWeight: FontWeight.bold,
        //         fontSize: 12.0,
        //       ),
        //     ),
        //     onPressed: () {},
        //   ),
        // );

        Widget loginButton = Container(
          // padding: EdgeInsets.only(
          //   bottom: 50.0,
          // ),
          width: 180.0,
          height: 50.0,
          child: MaterialButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            color: kColorPrimary,
            child: Text(
              S().login.toUpperCase(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              _login();
            },
          ),
        );

        return Observer(
          builder: (_) => LoaderHUD(
            inAsyncCall: loginStore.isLoginLoading,
            child: Scaffold(
              body: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(kBackground),
                    fit: BoxFit.cover,
                  ),
                ),
                child: ListView(
                  children: [
                    SizedBox(height: 30.0),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: Image.asset(
                        kLogoImage,
                        width: 250.0,
                        height: 250.0,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 30.0,
                        right: 30.0,
                        bottom: 50.0,
                      ),
                      child: Card(
                        elevation: 10.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        shadowColor: Colors.black,
                        child: Column(
                          children: [
                            welcomeLabel,
                            phoneNumberTextField,
                            loginButton,
                            SizedBox(
                              height: 50.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
