import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/db_const.dart';
import 'package:finance_apps/constants/images.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/helper/login_store.dart';
import 'package:finance_apps/screens/cashflow.dart';
import 'package:finance_apps/widgets/drawer_menu.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'cashflow.dart';

class HomePage extends StatefulWidget {

  final User user;

  HomePage({
    this.user,
  });

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  String phoneNumber;
  String userName = '';
  bool isUserExist = false;
  List listUser = [];
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection(kUserTable);
  final CollectionReference saldoCollection =
      FirebaseFirestore.instance.collection(kSaldoTable);

  @override
  void initState() {
    super.initState();
    phoneNumber = widget.user.phoneNumber;
    getUserProfile();
  }

  Future getUserProfile() async {
    await userCollection.get().then((value) {
      var query = value.docs;
      for (int i = 0; i < query.length; i++) {
        listUser.add(query[i].id);
        if (phoneNumber == listUser[i]) {
          isUserExist = true;
        }
      }
      if (!isUserExist) {
        createUser();
      }
    });
  }

  Future createUser() async {
    await userCollection.doc(phoneNumber).set({
      S().id : widget.user.uid,
      S().noHP : phoneNumber,
      S().nama : '',
      S().peran : S().tidakMemilih,
      S().tanggalLahir : DateFormat(kDateFormat).format(DateTime.now()),
    });
    await saldoCollection.doc(phoneNumber).set({
      S().totalSaldo : 0,
    });
  }

  @override
  Widget build(BuildContext context) {

    // double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Consumer<LoginStore>(
      builder: (_, loginStore, __) {
        return DefaultTabController(
          length: 3,
          initialIndex: 0,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: kColorPrimary,
              title: Container(
                child: Image.asset(
                  kLogoImageLong,
                  height: screenHeight * 0.045,
                ),
              ),
              centerTitle: true,
              shadowColor: Colors.blue,
              elevation: 10.0,
              bottom: TabBar(
                indicatorColor: Colors.red,
                tabs: <Widget>[
                  Tab(
                    text: S().semua,
                    icon: Icon(Icons.bar_chart_sharp),
                  ),
                  Tab(
                    text: S().pemasukan,
                    icon: Icon(Icons.attach_money),
                  ),
                  Tab(
                    text: S().pengeluaran,
                    icon: Icon(Icons.money_off),
                  ),
                  
                ],
              ),
            ),
            drawer: DrawerMenu(
              loginStore: loginStore, 
              user: widget.user,
            ),
            body: TabBarView(
              children: <Widget>[
                CashFlow(
                  jenis: S().semua,
                  phoneNumber: phoneNumber,
                ),
                CashFlow(
                  jenis: S().pemasukan,
                  phoneNumber: phoneNumber,
                ),
                CashFlow(
                  jenis: S().pengeluaran,
                  phoneNumber: phoneNumber,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
