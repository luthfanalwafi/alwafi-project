const kBackground = 'assets/images/background.jpeg';
const kBackgroundGreen = 'assets/images/background-green.jpg';
const kLogoImage = 'assets/images/aa-logo.png';
const kLogoImageLong = 'assets/images/aa-logo-long.png';
const kFotoMama = 'assets/images/ily-mama.jpeg';
const kFotoPapa = 'assets/images/ily-papa.jpg';