import 'package:intl/intl.dart';

import 'library.dart';

const kDateFormat = 'dd MMMM yyyy';
const kDateFormatDB = 'yyyy MM dd HH:mm:ss';
const kDateFormatField = 'dd MMMM yyyy HH:mm:ss';
const kUserTable = 'userProfile';
const kCashFlowTable = 'cashflow2';
const kSaldoTable = 'saldo';
var kListPeran = [
  S().tidakMemilih,
  S().suami,
  S().istri,
];
final kFormatCurrency = NumberFormat.simpleCurrency(
  locale: 'id_ID',
  decimalDigits: 0,
);
