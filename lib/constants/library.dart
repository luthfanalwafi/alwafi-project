import 'package:intl/intl.dart';

class S {
  S();

  /// `'Afi ❤️ Anis'`
  String get afianis {
    return Intl.message(
      'Afi ❤️ Anis',
      name: 'Afi❤️Anis',
      desc: '',
      args: [],
    );
  }

  /// `Saldo`
  String get saldo {
    return Intl.message(
      'Saldo',
      name: 'Saldo',
      desc: '',
      args: [],
    );
  }

  /// `Total Saldo`
  String get totalSaldo {
    return Intl.message(
      'Total Saldo',
      name: 'TotalSaldo',
      desc: '',
      args: [],
    );
  }

  /// `Saldo Nganggur`
  String get saldoNganggur {
    return Intl.message(
      'Saldo Nganggur',
      name: 'SaldoNganggur',
      desc: '',
      args: [],
    );
  }

  /// `Semua`
  String get semua {
    return Intl.message(
      'Semua',
      name: 'Semua',
      desc: '',
      args: [],
    );
  }

  /// `Pemasukan`
  String get pemasukan {
    return Intl.message(
      'Pemasukan',
      name: 'Pemasukan',
      desc: '',
      args: [],
    );
  }

  /// `Pengeluaran`
  String get pengeluaran {
    return Intl.message(
      'Pengeluaran',
      name: 'Pengeluaran',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get ok {
    return Intl.message(
      'OK',
      name: 'ok?',
      desc: '',
      args: [],
    );
  }

  /// `Keluar`
  String get keluar {
    return Intl.message(
      'Keluar',
      name: 'Keluar',
      desc: '',
      args: [],
    );
  }

  /// `Ooops`
  String get ooops {
    return Intl.message(
      'Ooops',
      name: 'Ooops',
      desc: '',
      args: [],
    );
  }

  /// `Sayang mau keluar?`
  String get konfirmasiKeluar {
    return Intl.message(
      'Sayang mau keluar?',
      name: 'Sayangmaukeluar?',
      desc: '',
      args: [],
    );
  }

  /// `Iya`
  String get iya {
    return Intl.message(
      'Iya',
      name: 'Iya',
      desc: '',
      args: [],
    );
  }

  /// `Gajadi`
  String get gajadi {
    return Intl.message(
      'Gajadi',
      name: 'Gajadi',
      desc: '',
      args: [],
    );
  }

  /// `Tambah`
  String get tambah {
    return Intl.message(
      'Tambah',
      name: 'Tambah',
      desc: '',
      args: [],
    );
  }

  /// `Keperluan`
  String get keperluan {
    return Intl.message(
      'Keperluan',
      name: 'Keperluan',
      desc: '',
      args: [],
    );
  }

  /// `Jumlah`
  String get jumlah {
    return Intl.message(
      'Jumlah',
      name: 'Jumlah',
      desc: '',
      args: [],
    );
  }

  /// `Ditambahkan Oleh`
  String get ditambahkanOleh {
    return Intl.message(
      'Ditambahkan Oleh',
      name: 'DitambahkanOleh',
      desc: '',
      args: [],
    );
  }

  /// `Ditambahkan Pada`
  String get ditambahkanPada {
    return Intl.message(
      'Ditambahkan Pada',
      name: 'DitambahkanPada',
      desc: '',
      args: [],
    );
  }

  /// `Tipe`
  String get tipe {
    return Intl.message(
      'Tipe',
      name: 'Tipe',
      desc: '',
      args: [],
    );
  }

  /// `Gagal`
  String get gagal {
    return Intl.message(
      'Gagal',
      name: 'gagal',
      desc: '',
      args: [],
    );
  }

  /// `Nomor HP tidak boleh kosong`
  String get noPhoneNumber {
    return Intl.message(
      'Nomor HP tidak boleh kosong',
      name: 'nomorhptidakbolehkosong',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'Logout',
      desc: '',
      args: [],
    );
  }

  /// `Selamat Datang Sayang ❤️❤️❤️`
  String get selamatDatang {
    return Intl.message(
      'Selamat Datang Sayang ❤️❤️❤️',
      name: 'SelamatDatangSayang❤️❤️❤️',
      desc: '',
      args: [],
    );
  }

  /// `No. HP`
  String get noHP {
    return Intl.message(
      'No. HP',
      name: 'noHP',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Nama`
  String get nama {
    return Intl.message(
      'Nama',
      name: 'Nama',
      desc: '',
      args: [],
    );
  }

  /// `ID`
  String get id {
    return Intl.message(
      'ID',
      name: 'ID',
      desc: '',
      args: [],
    );
  }

  /// `Peran`
  String get peran {
    return Intl.message(
      'Peran',
      name: 'Peran',
      desc: '',
      args: [],
    );
  }

  /// `Tidak Memilih`
  String get tidakMemilih {
    return Intl.message(
      'Tidak Memilih',
      name: 'TidakMemilih',
      desc: '',
      args: [],
    );
  }

  /// `Suami`
  String get suami {
    return Intl.message(
      'Suami',
      name: 'Suami',
      desc: '',
      args: [],
    );
  }

  /// `Istri`
  String get istri {
    return Intl.message(
      'Istri',
      name: 'Istri',
      desc: '',
      args: [],
    );
  }

  /// `Tanggal Lahir`
  String get tanggalLahir {
    return Intl.message(
      'Tanggal Lahir',
      name: 'Tanggal Lahir',
      desc: '',
      args: [],
    );
  }

  /// `Ubah`
  String get ubah {
    return Intl.message(
      'Ubah',
      name: 'Ubah',
      desc: '',
      args: [],
    );
  }

  /// `Hapus`
  String get hapus {
    return Intl.message(
      'Hapus',
      name: 'Hapus',
      desc: '',
      args: [],
    );
  }

  /// `Hapus Data`
  String get hapusData {
    return Intl.message(
      'Hapus Data',
      name: 'HapusData',
      desc: '',
      args: [],
    );
  }

  /// `Yakin mau dihapus?`
  String get konfirmasiHapus {
    return Intl.message(
      'Yakin mau dihapus?',
      name: 'Yakinmaudihapus?',
      desc: '',
      args: [],
    );
  }

  /// `Lupa Password?`
  String get lupaPassword {
    return Intl.message(
      'Lupa Password?',
      name: 'lupapassword?',
      desc: '',
      args: [],
    );
  }

  /// `berhasil diubah!`
  String get berhasilDiubah {
    return Intl.message(
      'berhasil diubah!',
      name: 'berhasildiubah!',
      desc: '',
      args: [],
    );
  }

  /// `Silakan isi semua kolom isian yang tersedia`
  String get isiKolom {
    return Intl.message(
      'Silakan isi semua kolom isian yang tersedia',
      name: 'Silakanisisemuakolomisianyangtersedia',
      desc: '',
      args: [],
    );
  }

  /// `Atur Saldo`
  String get aturSaldo {
    return Intl.message(
      'Atur Saldo',
      name: 'AturSaldo',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get profile {
    return Intl.message(
      'Profile',
      name: 'Profile',
      desc: '',
      args: [],
    );
  }

  /// `Pengaturan`
  String get pengaturan {
    return Intl.message(
      'Pengaturan',
      name: 'Pengaturan',
      desc: '',
      args: [],
    );
  }

  /// `TextField`
  String get textField {
    return Intl.message(
      'TextField',
      name: 'TextField',
      desc: '',
      args: [],
    );
  }

  /// `DropDown`
  String get dropDown {
    return Intl.message(
      'DropDown',
      name: 'DropDown',
      desc: '',
      args: [],
    );
  }

  /// `DatePicker`
  String get datePicker {
    return Intl.message(
      'DatePicker',
      name: 'DatePicker',
      desc: '',
      args: [],
    );
  }

  /// `Tambah Tabungan`
  String get tambahTabungan {
    return Intl.message(
      'Tambah Tabungan',
      name: 'TambahTabungan',
      desc: '',
      args: [],
    );
  }

  /// `Tarik untuk memuat`
  String get tarikUntukMemuat {
    return Intl.message(
      'Tarik untuk memuat',
      name: 'Tarikuntukmemuat',
      desc: '',
      args: [],
    );
  }

  /// `Gagal memuat!`
  String get gagalMemuat {
    return Intl.message(
      'Gagal memuat!',
      name: 'Gagalmemuat',
      desc: '',
      args: [],
    );
  }

  /// `Lepas untuk memuat`
  String get lepasUntukMemuat {
    return Intl.message(
      'Lepas untuk memuat',
      name: 'Lepasuntukmemuat',
      desc: '',
      args: [],
    );
  }

  /// `Tidak ada data`
  String get tidakAdaData {
    return Intl.message(
      'Tidak ada data',
      name: 'Tidakadadata',
      desc: '',
      args: [],
    );
  }
}