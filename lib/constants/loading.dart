import 'package:finance_apps/constants/library.dart';
import 'package:flutter/material.dart';
// import 'package:pull_to_refresh/pull_to_refresh.dart';


/// For Loading Widget
Widget kLoadingWidget(context) => Center(
      child: Image.asset(
        'assets/images/loading.gif',
        height: 70.0,
        width: 70.0,
        fit: BoxFit.cover,
      ),
    );

// Widget kCustomFooter(context) => CustomFooter(
//       builder: (BuildContext context, LoadStatus mode) {
//         Widget body;
//         if (mode == LoadStatus.idle) {
//           body = Text(S().tarikUntukMemuat);
//         } else if (mode == LoadStatus.loading) {
//           body = kLoadingWidget(context);
//         } else if (mode == LoadStatus.failed) {
//           body = Text(S().gagalMemuat);
//         } else if (mode == LoadStatus.canLoading) {
//           body = Text(S().lepasUntukMemuat);
//         } else {
//           body = Text(S().tidakAdaData);
//         }
//         return Container(
//           height: 55.0,
//           child: Center(child: body),
//         );
//       },
//     );
