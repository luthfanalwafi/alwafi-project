import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/screens/home_page.dart';
import 'package:finance_apps/screens/login_page.dart';
import 'package:finance_apps/screens/otp_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'login_helper.dart';

class LoginStore = LoginStoreBase with _$LoginStore;

abstract class LoginStoreBase with Store {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String actualCode;

  @observable
  bool isLoginLoading = false;
  @observable
  bool isOtpLoading = false;

  @observable
  GlobalKey<ScaffoldState> loginScaffoldKey = GlobalKey<ScaffoldState>();
  @observable
  GlobalKey<ScaffoldState> otpScaffoldKey = GlobalKey<ScaffoldState>();

  @observable
  User firebaseUser;

  @action
  Future<User> isAlreadyAuthenticated() async {
    firebaseUser = _auth.currentUser;
    return firebaseUser;
  }

  @action
  Future<void> getCodeWithPhoneNumber(
      BuildContext context, String phoneNumber) async {
    isLoginLoading = true;

    // await _auth.verifyPhoneNumber().

    await _auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      timeout: const Duration(seconds: 5),
      verificationCompleted: (AuthCredential auth) async {
        await _auth.signInWithCredential(auth).then(
          (UserCredential value) {
            if (value != null && value.user != null) {
              print('Authentication successful');
              onAuthenticationSuccessful(context, value);
            } else {
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  AlertDialog alert = AlertDialog(
                    title: Text(S().gagal),
                    content: Text(
                      S().noPhoneNumber,
                      style: const TextStyle(
                        height: 1.5,
                      ),
                    ),
                    actions: <Widget>[
                      MaterialButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          S().ok,
                          style: const TextStyle(
                            color: kColorPrimary,
                          ),
                        ),
                      ),
                    ],
                  );
                  return alert;
                },
              );
            }
          },
        ).catchError(
          (error) {
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                AlertDialog alert = AlertDialog(
                  title: Text(S().gagal),
                  content: Text(
                    error,
                    style: const TextStyle(
                      height: 1.5,
                    ),
                  ),
                  actions: <Widget>[
                    MaterialButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        S().ok,
                        style: const TextStyle(
                          color: kColorPrimary,
                        ),
                      ),
                    ),
                  ],
                );
                return alert;
              },
            );
          },
        );
      },
      verificationFailed: (FirebaseAuthException authException) {
        print('Error message: ' + authException.message);
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            AlertDialog alert = AlertDialog(
              title: Text(S().gagal),
              content: Text(
                authException.message,
                style: const TextStyle(
                  height: 1.5,
                ),
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    S().ok,
                    style: const TextStyle(
                      color: kColorPrimary,
                    ),
                  ),
                ),
              ],
            );
            return alert;
          },
        );
        isLoginLoading = false;
      },
      codeSent: (String verificationId, [int forceResendingToken]) async {
        actualCode = verificationId;
        isLoginLoading = false;
        await Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => OtpPage()));
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        actualCode = verificationId;
      },
    );
  }

  @action
  Future<void> validateOtpAndLogin(BuildContext context, String smsCode) async {
    isOtpLoading = true;
    final AuthCredential _authCredential = PhoneAuthProvider.credential(
        verificationId: actualCode, smsCode: smsCode);

    await _auth.signInWithCredential(_authCredential).catchError((error) {
      isOtpLoading = false;
      otpScaffoldKey.currentState.showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.red,
        content: Text(
          'Wrong code ! Please enter the last code received.',
          style: TextStyle(color: Colors.white),
        ),
      ));
    }).then((UserCredential authResult) {
      if (authResult != null && authResult.user != null) {
        print('Authentication successful');
        onAuthenticationSuccessful(context, authResult);
      }
    });
  }

  Future<void> onAuthenticationSuccessful(
      BuildContext context, UserCredential result) async {
    isLoginLoading = true;
    isOtpLoading = true;

    firebaseUser = result.user;

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => HomePage(user: firebaseUser)),
        (Route<dynamic> route) => false);

    isLoginLoading = false;
    isOtpLoading = false;
  }

  @action
  Future<void> signOut(BuildContext context) async {
    await _auth.signOut();
    await Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => LoginPage()),
        // MaterialPageRoute(builder: (_) => HomePage()),
        (Route<dynamic> route) => false);
    firebaseUser = null;
  }
}
