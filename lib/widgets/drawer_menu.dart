import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/images.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/screens/atur_saldo.dart';
import 'package:finance_apps/screens/user_profile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class DrawerMenu extends StatelessWidget {

  final loginStore;
  final User user;

  DrawerMenu({
    this.loginStore,
    this.user,
  });

  @override
  Widget build(BuildContext context) {
    // double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final logo = Container(
      margin: EdgeInsets.only(bottom: 20.0),
      height: screenHeight * 0.2,
      alignment: Alignment.center,
      child: Image.asset(
        kLogoImage,
        fit: BoxFit.cover,
      ),
    );

    void logout() {
      Navigator.of(context).pop();
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S().ooops),
            content: Text(
              S().konfirmasiKeluar,
              style: TextStyle(
                height: 1.5,
              ),
            ),
            actions: <Widget>[
              MaterialButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  S().gajadi,
                  style: TextStyle(
                    color: kColorPrimary,
                  ),
                ),
              ),
              MaterialButton(
                onPressed: () {
                  loginStore.signOut(context);
                },
                child: Text(
                  S().iya,
                  style: TextStyle(
                    color: kColorPrimary,
                  ),
                ),
              ),
            ],
          );
        },
      );
    }

    Widget drawerMenu(icon, String title, Function function) {
      return Column(
        children: [
          Container(
            child: ListTile(
              leading: Icon(icon),
              trailing: Icon(Icons.arrow_forward_ios),
              title: Text(title),
              onTap: function,
            ),
          ),
          Divider(),
        ],
      );
    }

    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(
                top: 50,
                left: 20.0,
                right: 20.0,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    logo,
                    drawerMenu(
                      Icons.attach_money,
                      S().aturSaldo,
                      () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return AturSaldo(
                                //ToDo
                              );
                            } 
                          ),
                        );
                      },
                    ),
                    drawerMenu(
                      Icons.person,
                      S().profile,
                      () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return UserProfile(
                                user: user,
                              );
                            } 
                          ),
                        );
                      },
                    ),
                    drawerMenu(
                      Icons.logout,
                      S().keluar,
                      () {
                        logout();
                      },
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
