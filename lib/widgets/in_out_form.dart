import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/db_const.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class InOutForm extends StatefulWidget {
  final tipe;
  final user;
  final Function onSubmit;

  InOutForm({
    this.tipe,
    this.user,
    this.onSubmit,
  });

  @override
  _InOutFormState createState() => _InOutFormState();
}

class _InOutFormState extends State<InOutForm> {
  TextEditingController keperluanController = TextEditingController();
  TextEditingController jumlahController = TextEditingController();
  final CollectionReference collection =
      FirebaseFirestore.instance.collection(kCashFlowTable);

  Future saveCashFlow() async {
    String date = DateFormat(kDateFormatDB).format(DateTime.now());
    String dateField = DateFormat(kDateFormatField).format(DateTime.now());
    int jumlah = int.parse(jumlahController.text);
    await collection.doc(date).set({
      S().ditambahkanOleh: widget.user,
      S().ditambahkanPada: dateField,
      S().jumlah: jumlah,
      S().keperluan: keperluanController.text,
      S().tipe: widget.tipe,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              bottom: 10.0,
            ),
            alignment: Alignment.centerLeft,
            child: Text(S().keperluan + ' : '),
          ),
          Container(
            padding: EdgeInsets.only(
              bottom: 15.0,
            ),
            child: TextFormField(
              decoration: kMessageTextField.copyWith(
                hintText: S().keperluan,
              ),
              controller: keperluanController,
              cursorColor: kColorPrimary,
              autocorrect: false,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              bottom: 10.0,
            ),
            alignment: Alignment.centerLeft,
            child: Text(S().jumlah + ' : '),
          ),
          Container(
            padding: EdgeInsets.only(
              bottom: 15.0,
            ),
            child: TextFormField(
              decoration: kMessageTextField.copyWith(
                hintText: S().jumlah,
              ),
              controller: jumlahController,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp("[0-9]"))
              ],
              cursorColor: kColorPrimary,
              autocorrect: false,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.only(
                  right: 10.0,
                ),
                child: MaterialButton(
                  color: kColorPrimary,
                  textColor: Colors.white,
                  child: Text(S().gajadi),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              Container(
                child: MaterialButton(
                  color: kColorPrimary,
                  textColor: Colors.white,
                  child: Text(S().tambah),
                  onPressed: () {
                    saveCashFlow();
                    Navigator.of(context).pop();
                    widget.onSubmit();
                  },
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
