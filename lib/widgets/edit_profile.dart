import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/db_const.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/constants/styles.dart';
import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  
  final title;
  final type;
  final nameController;
  final peran;
  final Function onClick;

  EditProfile({
    this.title,
    this.type,
    this.nameController,
    this.peran,
    this.onClick,
  });

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  String peran;
  double fontSize = 16.0;

  @override
  void initState() {
    super.initState();
    peran = widget.peran;
  }

  Widget editWidget() {
    switch (widget.type) {
      case 'TextField':
        return textField(widget.title);
        break;
      case 'DropDown':
        return dropDown(widget.title);
        break;
      default:
        return textField(widget.title);
    }
  }

  Container textField(title) {
    return Container(
      child: TextFormField(
        decoration: kMessageTextField.copyWith(
          hintText: title,
        ),
        controller: widget.nameController,
        cursorColor: kColorPrimary,
        autocorrect: false,
      ),
    );
  }

  Container dropDown(title) {
    return Container(
      width: MediaQuery.of(context).size.width,
      // height: 38.0,
      padding: EdgeInsets.only(left: 10.0),
      decoration: kDropDown,
      child: Theme(
        data: ThemeData(
          canvasColor: kDarkBgLight,
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            hint: Text(title),
            value: peran,
            elevation: 5,
            items: kListPeran.map((item) {
              return DropdownMenuItem(
                child: Text(item),
                value: item,
              );
            }).toList(),
            onChanged: (value) {
              setState(() {
                peran = value;
              });
            },
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 30.0,
        right: 30.0,
        top: 50.0,
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 5.0,
              bottom: 10.0,
            ),
            child: Text(
              widget.title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: fontSize,
              ),
            ),
          ),
          // EditProfile(
          //   title: title,
          //   type: type,
          //   nameController: nameController,
          //   peran: peran,
          //   peranFix: ddValue,
          // ),
          editWidget(),
          Container(
            padding: EdgeInsets.symmetric(
              vertical: 30.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                MaterialButton(
                  color: kColorPrimary,
                  textColor: Colors.white,
                  child: Text(S().gajadi),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(width: 20.0),
                MaterialButton(
                  color: kColorPrimary,
                  textColor: Colors.white,
                  child: Text(S().ubah),
                  onPressed: () {
                    Navigator.of(context).pop();
                    var value;
                    if (widget.type == S().textField) {
                      value = widget.nameController.text;
                    } else if (widget.type == S().dropDown) {
                      value = peran;
                    }
                    widget.onClick(
                      widget.title,
                      value,
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
