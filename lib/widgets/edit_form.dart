import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:finance_apps/constants/colors.dart';
import 'package:finance_apps/constants/db_const.dart';
import 'package:finance_apps/constants/library.dart';
import 'package:finance_apps/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class EditForm extends StatefulWidget {

  final tipe;
  final docID;
  final keperluan;
  final jumlah;
  final Function onSubmit;

  EditForm({
    this.tipe,
    this.docID,
    this.onSubmit,
    this.keperluan,
    this.jumlah,
  });

  @override
  _EditFormState createState() => _EditFormState();
}

class _EditFormState extends State<EditForm> {

  TextEditingController keperluanController = TextEditingController();
  TextEditingController jumlahController = TextEditingController();
  final CollectionReference collection =
      FirebaseFirestore.instance.collection(kCashFlowTable);

   @override
  void initState() {
    super.initState();
    keperluanController.text = widget.keperluan;
    jumlahController.text = widget.jumlah.toString();
  }

  Future editCashFlow() async {
    int jumlah = int.parse(jumlahController.text);
    await collection.doc(widget.docID).update({
      S().keperluan: keperluanController.text,
      S().jumlah : jumlah,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              bottom: 10.0,
            ),
            alignment: Alignment.centerLeft,
            child: Text(S().keperluan + ' : '),
          ),
          Container(
            padding: EdgeInsets.only(
              bottom: 15.0,
            ),
            child: TextFormField(
              decoration: kMessageTextField.copyWith(
                hintText: S().keperluan,
              ),
              controller: keperluanController,
              cursorColor: kColorPrimary,
              autocorrect: false,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              bottom: 10.0,
            ),
            alignment: Alignment.centerLeft,
            child: Text(S().jumlah + ' : '),
          ),
          Container(
            padding: EdgeInsets.only(
              bottom: 15.0,
            ),
            child: TextFormField(
              decoration: kMessageTextField.copyWith(
                hintText: S().jumlah,
              ),
              controller: jumlahController,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp("[0-9]"))
              ],
              cursorColor: kColorPrimary,
              autocorrect: false,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.only(
                  right: 10.0,
                ),
                child: MaterialButton(
                  color: kColorPrimary,
                  textColor: Colors.white,
                  child: Text(S().gajadi),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              Container(
                child: MaterialButton(
                  color: kColorPrimary,
                  textColor: Colors.white,
                  child: Text(S().ubah),
                  onPressed: () {
                    editCashFlow();
                    Navigator.of(context).pop();
                    widget.onSubmit();
                  },
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}